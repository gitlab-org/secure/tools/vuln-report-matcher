#!/bin/bash

# Expect 3 inputs. 
# 1. The analyser name. Eg gemnasium
# 2. The projects vuln report csv
# 3. The high_crit_vulns_csv.csv generated from parse_vulns.sh
if [ $# -ne 3 ]; then
    echo "Usage: $0 <analyser_name> <proj_vuln_report_csv> <high_crit_vulns_csv>"
    exit 1
fi

analyser_name=$1
proj_vuln_report_csv=$2
high_crit_vulns_csv=$3

# Check if the CSV files exist
if [ ! -f "$proj_vuln_report_csv" ]; then
    echo "Error: CSV file $proj_vuln_report_csv not found."
    exit 1
fi

if [ ! -f "$high_crit_vulns_csv" ]; then
    echo "Error: CSV file $high_crit_vulns_csv not found."
    exit 1
fi

# Read each row of high_crit_vulns_csv and extract CVE, package_name, and os
while IFS=',' read -r cve package_name os; do
    # Print the cve, package name and os from the high_crit_vulns_csv to help with visual matching
    printf "Details from customer report:\n"
    printf "cve: %s\npackage_name: %s\nos: %s\n" "$cve" "$package_name" "$os"
    printf "Vulns that match %s from proj vuln report:\n" "$cve"

    # Print the vuln status and the project vuln url
    while IFS=',' read -r status report_cve full_path; do
        if [ "$report_cve" == "$cve" ]; then
            # Remove trailing newlines or spaces from full_path and parse for the vuln ID
            proj_vuln_id=$(echo "$full_path" | tr -d '\n' | tr -d '\r' | sed 's/[^0-9]//g')
            # Print the vuln report link in markdown format for easy copy
            printf "[%s](https://gitlab.com/gitlab-org/security-products/analyzers/%s/-/security/vulnerabilities/%s)\n" "$status" "$analyser_name" "$proj_vuln_id"
        fi
    done < <(tail -n +2 "$proj_vuln_report_csv")  # Skip the header line of report_cs
    echo
done < "$high_crit_vulns_csv"
