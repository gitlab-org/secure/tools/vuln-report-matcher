#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <gl-analyser-report.json>"
    exit 1
fi

input_file=$1
output_file="high_crit_vulns.csv"

# Use jq to filter high and critical vulns and save the output to a variable
filtered_vulns=$(jq -c '.vulnerabilities[] | select(.severity == "High" or .severity == "Critical")' "$input_file")

num_vulns=$(echo "$filtered_vulns" | jq -s 'length')
echo "Found $num_vulns vulns"

# Loop through each vulnerability in filtered_vulns to extract the cve, package name and os and add to the output file
echo "$filtered_vulns" | while IFS= read -r vuln; do
    cve=$(echo "$vuln" | jq -r '.identifiers[].value')
    package=$(echo "$vuln" | jq -r '.location.dependency.package.name')
    os=$(echo "$vuln" | jq -r '.location.operating_system')
    echo "$cve,$package,$os" >> "$output_file"
done

# Print the result to csv file
cat "$output_file"