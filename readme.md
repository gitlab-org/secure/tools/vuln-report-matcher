# Vulnerability Report Matcher

## Background
Occasionally, customers conduct scans on our analyzers and seek clarification on the status of high and critical vulnerabilities.

To provide a response, we would typically extract the high and critical vulns from the customer's report and retrieve the status of these vulns from our vuln dashboard to respond to the customer.

## Manual steps used previously:
1. Download the customer-provided analyser json report.
1. Use jq to filter the Critical, High vulnerabilities and sort by CVE
    ```
    jq '[.vulnerabilities[] | select(.severity == "High" or .severity == "Critical")] | sort_by(.location.dependency.package.name)' ./example/gl-container-scanning-report.json
    ```
1. Go to the vulnerability report for the associated project
1. Export vulnerabilities to CSV
1. Open CSV in Numbers
1. Hide columns A-I
1. Add a quick filter on column J (CVE)
1. For each item in the customer report:
    1. Copy the CVE
    1. In the quick filter, uncheck all, paste the CVE and check just the CVE match
    1. Find the corresponding entry (there may be various since we scan multiple images)
    1. Copy the vulnerability ID from the Full Path column
    1. Paste into the browser location, appending it to the URL path to the vulnerability page
    1. Copy status into this issue

As this process is labor-intensive, the aim of this script is to streamline this process.

## Partial automated steps with scripts
1. Download the customer-provided analyser json report.
1. Run the `parse_vulns.sh` script to filter for high and critical vulns and it will save them to the `high-crit-vulns.csv` file
    ```
    chmod +x parse_vulns.sh
    ./parse_vulns.sh example/gl-container-scanning-report.json
    ```
1. Go to the vulnerability report for the associated project
1. Export vulnerabilities to CSV
1. Delete all columns except `Status`, `CVE` and `Full Path`
1. Run the `match_cve.sh` script and pass in the `analyser name`, `high-crit-vulns.csv` and the vuln report downloaded above.
    ```
    ./match_cve.sh gemnasium example/csv-export.csv high-crit-vulns.csv 
    ```
    This will print out a list of vulns that match the cves from `high-crit-vulns.csv`. Example:
    ```
    Details from customer report:
    cve: CVE-2023-28531
    package_name: openssh-client
    os: debian 12.5
    Vulns that match CVE-2023-28531 from proj vuln report:
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000019)
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000020)
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000021)
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000022)
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000023)
    [resolved](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/security/vulnerabilities/0000024)
    ```
7. **Manual step** Visit each of these links to find the vuln whose package name and OS match the details from the customer report
8. Share the status of the vuln together with any notes from the link with the customer or CS team.